console.log("Hello World!!!");

let person = {
	name: "Dennis Orpina",
	age: 37,
		friends:{
			Hoenn: ["Peter", "Parker"],
			Kanto: ["Spidey", "Man"]
		},
	pokemon:["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function(){
		console.log("Pikachu I choose you!")
	}
};
// 5. Initialize/Add the trainer object method named talk that prints out the message  PIKACHU! I Choose You!
console.log(person);

// 6. Access the trainer object properties using dot and square bracket notation.

// person.name = "Ash Ketchum";
console.log("Result of the dot notation: ");
console.log(person.name);

// Result of bracket notation

person["pokemon"] = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
console.log("Result of square bracket notation:");
console.log(person["pokemon"]);

console.log("This is the result of talk method");
person.talk();

// 7. Invoke/Call the trainer object method.

// 8. Create a constructor for creating a pokemon with the following properties:


function Pokemon(name,level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = level *2;
	this.attack = level;

// 10. Create a tackle method

	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		target.health -= this.attack;
		if (target.health <= 0){
			target.faint();
		}
		else {
			console.log(target.name + "'s health is now reduced to " + target.health);
		}
	}

	this.faint = function(){
		console.log(this.name + " fainted ");
	}
}
let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu)
mewtwo.tackle(geodude);
console.log(geodude);